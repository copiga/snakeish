#include <ncurses.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <string.h>

#define SPECIALCHANCETHING 79
#define STARTLENGTH 3

void delong(void);
void rewardsgen(void);
void specialgen(void);
void collisioncheck(void);
void scoresav(void);

/*required for collision checking*/
int x[100],y[100],maxx,maxy,rewardx,rewardy,score,specialy,specialx,length;

_Bool lost = false;
_Bool specialthere = false;

int main(void)
{
  
  int input,ldir=0;
  


  initscr();
  noecho();
  keypad(stdscr,TRUE);
 

  /*tels player how to play and waits for input before starting and setting input to halfdelay*/
  printw("use arrow keys or wasd");
  getch();
  mvprintw(0,0,"                       ");
  halfdelay(1);

  /*find centre of the screen and put snake head there*/
  getmaxyx(stdscr,maxy,maxx);
  x[1]=maxx/2;
  y[1]=maxy/2;
  mvprintw(y[1],x[1],"*");
  

  /*set the starting length of the snake, STARTLENGTH is done in a #define*/
  length = STARTLENGTH;
  
  /*generate rewards*/
  rewardsgen();


  refresh();



  while((input=getch()) != 27 && lost == false)
    {
      if(input == KEY_UP || input == 'w' || input == 'W')
	{
	  delong();
	  if(ldir != 1)
	    {
	  y[1] = y[2]-1;
	  mvprintw(y[1],x[1],"*");
	  ldir = 0;
	    }
	  else
	    {
	      y[1]=y[2]+1;
	      mvprintw(y[1],x[1],"*");
	    }
	  
	}
      else if(input == KEY_DOWN || input == 's' || input == 'S')
	{
	  delong();
	  if(ldir != 0)
	    {
	      y[1] = y[2]+1;
	      mvprintw(y[1],x[1],"*");
	      ldir = 1;
	    }
	  else
	    {
	      y[1]=y[2]-1;
	      mvprintw(y[1],x[1],"*");
	    }
	  
	   
      	}
      else if(input == KEY_LEFT || input == 'a' || input == 'A')
	{
	  delong();
	  if(ldir != 3)
	    {
	      x[1] = x[2]-1;
	      mvprintw(y[1],x[1],"*");
	      ldir = 2;
	    }
	  else
	    {
	      x[1]=x[2]+1;
	      mvprintw(y[1],x[1],"*");
	    }
	  
	}
      else if(input == KEY_RIGHT || input == 'd' || input == 'D')
	{
	  delong();
	  if(ldir != 2)
	    {
      	  x[1] = x[2]+1;
	  mvprintw(y[1],x[1],"*");
	  ldir = 3;
	    }
	  else
	    {
	      x[1]=x[2]-1;
	      mvprintw(y[1],x[1],"*");
	    }
	  
	}
      else if(ldir == 0)
	{
	  delong();
	  y[1] = y[2]-1;
	  mvprintw(y[1],x[1],"*");	  
	}
      else if(ldir == 1)
	{
	  delong();
	  y[1] = y[2]+1;
	  mvprintw(y[1],x[1],"*");
	}
      else if(ldir == 2)
	{
	  delong();
	  x[1] = x[2]-1;
	  mvprintw(y[1],x[1],"*");
	}
      else if(ldir == 3)
	{
	  delong();
	  x[1] = x[2]+1;
	  mvprintw(y[1],x[1],"*");
	}
      collisioncheck();
      specialgen();
      refresh();
    }


  endwin();
  printf("\n\n\nyour score was %d\n\n\n",score);
  scoresav();
  
  return 0;
}

void collisioncheck(void)
{
  /***************************
   *working 20:09 09/10/2012 *
   ***************************
   *checks whether anything  *
   *has the same coordinates *
   *as anything it shouldnt  *
   **************************/

  /*out of border*/
  if(x[1]>=maxx || y[2]>=maxy || x[1]<0 || y[2]<0)
    {
      lost = true;
    }


  /*hit rewards*/
    if(rewardy == y[1] && rewardx == x[1])
    {
      rewardsgen();
      score++;
      mvprintw(1,1,"%d",score);
      if(score%2==0)
	{
	  length++;
	}
      
    }

  /*hit special*/
  if(y[1] == specialy && x[1] == specialx)
    {
      specialthere = false;
      score = score+10;
      mvprintw(1,1,"%d",score);
      srand(time(NULL)+getpid());
      if(length>5)
	{
	  mvprintw(y[length],x[length]," ");
	  length--;
	}
      
    }

  /*bit tail*/
  int wherein;
  for(wherein=2;wherein<length;wherein++)
    {
      if(y[1]==y[wherein]&&x[1]==x[wherein]){lost=true;}
    }
}

void rewardsgen(void)
{
  /***************************
   *working 20:09 09/10/2012 *
   ***************************
   *generates a random number*
   *that will fit on screen  *
   *and sets it as '+'       *
   **************************/



  rewardx =rand()%maxx;
  rewardy =rand()%maxy;
  mvprintw(rewardy,rewardx,"+");
  specialgen();
}

void specialgen(void)
{
  /***************************
   *working 20:09 09/10/2012 *
   ***************************
   *generates a random number*
   *that will fit on screen  *
   *and sets it as '$'       *
   **************************/



  if(rand()%SPECIALCHANCETHING==0 && !specialthere)
    {
      specialthere = true;
      specialx=rand()%maxx;
      specialy=rand()%maxy;
      mvprintw(specialy,specialx,"$");
    }
}

void delong(void)
{

  /***************************
   *working 17:16 11/10/2012 *
   ***************************
   *stores x and y coords of *
   *previous head values and *
   *deletes the ones longer  *
   *than length              *
   **************************/
  
  
  int wherein;
  
  for(wherein = 99;wherein>1;wherein--)
    {
      
      y[wherein]=y[wherein-1];
      x[wherein]=x[wherein-1];
      
    }
  /*the +1 is temporary, i forgot that things are zero-indexed...*/
  mvprintw(y[length+1],x[length+1]," ");
}

void scoresav(void)
{
  char name[123];
  printf ("please enter your name\n");fflush(stdout);
  scanf ("%122s",&name);
  FILE *hiscore;
  hiscore = fopen("scores", "a+");
  
  fprintf (hiscore,"%s;\t;%d\n", name, score);
  fclose(hiscore);
}
